import React from 'react'

import StackNavigatorScreen from './src/StackNavigatorScreen'

export default function App() {
  return <StackNavigatorScreen />
}
