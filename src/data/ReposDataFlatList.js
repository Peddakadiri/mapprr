import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  FlatList,
  TouchableOpacity,
  Dimensions,
  Image,
  TouchableHighlight,
  AsyncStorage,
  Alert
} from 'react-native';
import { NavigationActions } from 'react-navigation';

class ReposDataFlatList extends Component {

  //Initialize the State
  constructor(props){
    super(props);
  }

  //Invoked before component is mounted
  componentWillMount() {

  }

  //Invoked immediately after a component is mounted
  componentDidMount() {
    console.log("ITEM", this.props.item);
  }

  //Invoked immediately before a component is unmounted and destroyed
  componentWillUnmount() {
  }

  //Main Content to display, which encapsulates all the components in it.
  render() {
    const { route, navigation } = this.props
    return (
      <TouchableOpacity
      onPress={() => navigation.navigate('RepoDetailsScreen', {
        "repoDetails": this.props.item
      })}
      >
      <View>
      <View style={{
        flexDirection: 'row'
      }}>
      <Image
      style={{
        height:150,
        width:150,
        margin:5,
        alignSelf:'flex-start',
        resizeMode:"contain",
        borderRadius: 20,
      }}
      source={ {uri: this.props.item.owner.avatar_url}}
      />
      <View style={{
        flex: 1,
        justifyContent:'center'
      }}>
      <Text style={{
        fontSize: 25,
        fontWeight: 'bold',
        marginLeft: 10,
      }}>{this.props.item.name} </Text>
      <Text style={{
        fontSize: 18,
        marginLeft: 10,
      }}>{this.props.item.full_name}</Text>
      <Text style={{
        fontSize: 15,
        marginLeft: 10,
        color: '#bebebe',
      }}>{`${this.props.item.watchers_count} Watchers`}</Text>
      </View>
      </View>
      </View>
      </TouchableOpacity>
    )
  }
}

export default ReposDataFlatList;
