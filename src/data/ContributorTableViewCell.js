import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  FlatList,
  TouchableOpacity,
  Dimensions,
  Image,
  TouchableHighlight,
  AsyncStorage,
  Alert
} from 'react-native';
import { NavigationActions } from 'react-navigation';

class ContributorTableViewCell extends Component {

  //Initialize the State
  constructor(props){
    super(props);
  }

  //Invoked before component is mounted
  componentWillMount() {

  }

  //Invoked immediately after a component is mounted
  componentDidMount() {

  }

  //Invoked immediately before a component is unmounted and destroyed
  componentWillUnmount() {
  }

  //Main Content to display, which encapsulates all the components in it.
  render() {
    const { route, navigation } = this.props
    return (
      <TouchableHighlight
      onPress={() => navigation.navigate('ContributorScreen', {
        "repoName": this.props.item
      })}
      >
      <View style={{
        flex:1,
        flexDirection: 'row',
      }}>
      <Image
      style={{
        height:150,
        width:200,
        margin:5,
        alignSelf:'center',
        resizeMode:"cover",
        borderRadius: 20
      }}
      source={ {uri: this.props.item.avatar_url}}
      />
      </View>
      </TouchableHighlight>
    )
  }
}

export default ContributorTableViewCell;
