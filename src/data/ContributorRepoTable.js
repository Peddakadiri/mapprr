import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  FlatList,
  TouchableOpacity,
  Dimensions,
  Image,
  TouchableHighlight,
  AsyncStorage,
  Alert
} from 'react-native';
import { NavigationActions } from 'react-navigation';

class ContributorRepoTable extends Component {

  //Initialize the State
  constructor(props){
    super(props);
  }

  //Main Content to display, which encapsulates all the components in it.
  render() {
    const { route, navigation } = this.props
    return (
      <TouchableOpacity
      onPress={() => navigation.navigate('RepoDetailsScreen', {
        "repoDetails": this.props.item
      })}>
      <View style={{backgroundColor: 'white'}}>
      <View style={{
        flexDirection: 'row',
        margin: 5
      }}>
      <Image
      style={{
        height:150,
        width:150,
        margin:5,
        alignSelf:'flex-start',
        borderRadius: 20,
        resizeMode:"contain"
      }}
      source={ {uri: this.props.item.owner.avatar_url}}
      />
      <View style={{
        flex: 1,
        justifyContent:'center'
      }}>
      <Text style={{
        fontSize: 25,
        marginLeft: 10,
        fontWeight: 'bold'
      }}>{this.props.item.name} </Text>
      <Text style={{
        fontSize: 20,
        marginLeft: 10,
      }}>{this.props.item.full_name}</Text>
      <Text style={{
        fontSize: 15,
        marginLeft: 10,
        color: '#bebebe',
      }}>{`${this.props.item.watchers_count} Watchers`}</Text>
      </View>
      </View>
      </View>
      </TouchableOpacity>
    )
  }
}

export default ContributorRepoTable;
