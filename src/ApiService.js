
var BaseURL = 'https://api.github.com/'
var ApiService = {
  //to search repositories
  "SearchRepositories" : `${BaseURL}search/repositories`,
  //get repositories
  "GetRepositories" : `${BaseURL}users/`,
  //get contributors
  "GetContributors" : `${BaseURL}repos/`,
  //get repositories of contributors
  "GetContributorRepos" : `${BaseURL}users/`,

}

module.exports = ApiService;
