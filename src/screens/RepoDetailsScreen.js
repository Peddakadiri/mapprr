import React, { Component } from 'react';
import {
  View,
  FlatList,
  Text,
  StyleSheet,
  ScrollView,
  Button,
  TextInput,
  Linking,
  TouchableOpacity,
  Alert,
  Image,
} from 'react-native';
import ApiService from '../ApiService';
import { SearchBar } from 'react-native-elements';
import ContributorTableViewCell from '../data/ContributorTableViewCell';
import { WebView } from 'react-native-webview';

class RepoDetailsScreen extends React.PureComponent{

  //Default Navigation Of Screen
  static navigationOptions = {
    headerTitle:'GitRepo',
    headerTitleStyle: {
      color:'#22A7F0'
    },
    headerStyle: {
      backgroundColor:'white'
    },
    headerTintColor: 'black',
  }

  constructor() {
    super();
    this.state = {
      page: 1,
      count: 10,
      contributorData: [],
    }
  }

  componentDidMount() {
    this.GetContributors()
  }

  render() {
    return(
      <ScrollView>
      <View style={{
        flex:1,
        backgroundColor: 'white'
      }}>
      <Text style={{
        fontSize: 30,
        margin: 10,
        fontWeight: 'bold'
      }}> {this.props.navigation.state.params.repoDetails.name} </Text>
      <Image
      style={{
        width: 400,
        height: 250,
        alignSelf: 'center',
        resizeMode:"cover",
        borderRadius: 20,
        marginTop: 10
      }}
      source={ {uri: this.props.navigation.state.params.repoDetails.owner.avatar_url}}
      />
      <View style={{
        flexDirection: 'row'
      }}>
      <Text style={{
        fontSize: 18,
        margin: 10,
        flex: 0.3,
        fontWeight: 'bold'
      }}> Name:  </Text>
      <Text style={{
        fontSize: 20,
        margin: 10,
        flex: 0.7
      }}> {this.props.navigation.state.params.repoDetails.name}  </Text>
      </View>
      <View style={{
        flexDirection: 'row'
      }}>
      <Text style={{
        fontSize: 18,
        margin: 10,
        flex: 0.3,
        fontWeight: 'bold'
      }}> Project Link:  </Text>
      <TouchableOpacity style={{
        margin: 10,
        flex: 0.7
      }}
      onPress={() => Linking.openURL(this.props.navigation.state.params.repoDetails.html_url)}>
      <Text style={{
        color: '#22A7F0'
      }}>{this.props.navigation.state.params.repoDetails.html_url}</Text>
      </TouchableOpacity>
      </View>
      <View style={{
        flexDirection: 'row'
      }}>
      <Text style={{
        fontSize: 18,
        margin: 10,
        flex: 0.3,
        fontWeight: 'bold'
      }}> Description:  </Text>
      <Text style={{
        fontSize: 20,
        margin: 10,
        flex: 0.7
      }}> {this.props.navigation.state.params.repoDetails.description}  </Text>
      </View>
      <Text style={{
        fontSize: 25,
        marginLeft: 10,
        marginTop: 20,
        fontWeight: 'bold',
        marginBottom: 20,
      }}> Contributors </Text>
      {this._renderContributorFlatList()}
      </View>
      </ScrollView>
    )
  }

  _renderContributorFlatList(){
    if ((this.state.contributorData != null) && (this.state.contributorData.length > 0)){
      return(
        <FlatList
        data={this.state.contributorData}
        numColumns={2}
        renderItem = {({item, index}) => {
          return (
            <ContributorTableViewCell
            item={item} index={index} navigation={this.props.navigation}
            >
            </ContributorTableViewCell>
          );
        }}
        keyExtractor={item => item._id}
        onEndReached={this.allAssetshandleLoadMore}
        onEndReachedThreshold={0}
        onEndThreshold={0}
        >
        </FlatList>
      )
    }else{
      return(
        <Text style={{
          color: '#95A5A6',
          justifyContent: 'center',
          fontSize: 20,
          fontWeight: '700',
          textAlign: 'center',
          marginTop: 50
        }}>Please wait...</Text>
      )
    }
  }

  async GetContributors(){
    try {
      fetch(`${ApiService.GetContributors}${this.props.navigation.state.params.repoDetails.owner.login}/${this.props.navigation.state.params.repoDetails.name}/contributors?page=${this.state.page}&per_page=${this.state.count}`, {
        method: 'GET',
      }).then((response) => response.json())
      .then(async (responseJson) => {
        console.log("responseJson", responseJson);
        if (responseJson.message == 'Not Found'){
          Alert.alert(
            `Not Found!!`,
            `Repo not found, please try for another.`,
            [
              {text: 'OK', onPress: () => console.log("not found")},
            ],
            { cancelable: false }
          )
        }else{
          this.setState({
            contributorData: this.state.page === 1 ? responseJson : [...this.state.contributorData, ...responseJson],
          })
        }
      })
      .catch((error) => {
        console.log(error)
      });
    } catch (error) {
      console.log(error)
    }
  }
}

export default RepoDetailsScreen;
