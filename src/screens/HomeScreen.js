import React, { Component } from 'react';
import {
  View,
  FlatList,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  Image,
} from 'react-native';
import ApiService from '../ApiService';
import ReposDataFlatList from '../data/ReposDataFlatList';

class HomeScreen extends React.PureComponent{

  //Default Navigation Of Screen
  static navigationOptions = {
    headerTitle:'GitRepo',
    headerTitleStyle: {
      color:'#22A7F0'
    },
    headerStyle: {
      backgroundColor:'white'
    },
    headerTintColor: 'black',
  }

  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      count: 10,
      searchGitRep: '',
      reposData: [],
      isLoaderEnabled: false,
    }
  }

  componentDidMount() {

  }

  render() {
    return(
      <View style={{backgroundColor: 'white', flex: 1}}>
      <View>
      <View style={{
        flexDirection: 'row',
        borderColor: 'white',
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        height: 40,
        margin: 5,
      }}>
      <TextInput style={{
        paddingLeft: 20,
        flex: 0.8,
        backgroundColor: '#E8E8E8',
        borderRadius: 10
      }}
      onChangeText={(text) =>{
        this.setState({searchGitRep: text})
      }
    }
    returnKeyType = "search"
    onSubmitEditing={() => { this.SearchFilterFunction(this.state.searchGitRep) }}
    value={this.state.text}
    underlineColorAndroid="transparent"
    placeholder="Search"
    />
    <TouchableOpacity
    onPress={() => this.SearchFilterFunction(this.state.searchGitRep)}
    style={{
      height: 40,
      backgroundColor: '#22A7F0',
      justifyContent: 'center',
      width: 100,
      alignItems: 'center',
      alignSelf: 'center',
      borderRadius: 10,
      flex: 0.2,
      marginLeft: 5,
      shadowColor: '#22A7F0',
      shadowOpacity: 0.7,
      shadowOffset: {width: 0,height: 1},
      shadowRadius: 5
    }}>
    <Text
    style={{
      width: 100,
      height: 40,
      textAlign: 'center',
      marginTop: 15,
      color: 'white',
      fontSize: 17,
    }}> Search </Text>
    </TouchableOpacity>
    </View>
    {this._renderDealsList()}
    </View>
    {this._renderLoader()}
    </View>
  )
}

//Enable OR Disable Loader
_renderLoader() {
  if (this.state.isLoaderEnabled == true) {
    return (
      <View style={{
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        alignItems: 'center',
        justifyContent: 'center'
      }}>
      <Image source={require('../images/multipleCircleLoader.gif')} />
      </View>
    )
  } else {
    return null
  }
}

async SearchFilterFunction(text) {
  if(text == ''){
    Alert.alert(
      `Please enter any github repo`,
      ``,
      [
        {text: 'OK', onPress: () => console.log("not found")},
      ],
      { cancelable: false }
    )
  }else{
    this.setState({
      page: 1,
      isFirstTimeCalculated: true
    })
    await this.GetRepositories()
  }
}

_renderDealsList(){
  if ((this.state.reposData != null) && (this.state.reposData.length > 0)){
    return(
      <FlatList style={{
        marginBottom: 50
      }}
      data={this.state.reposData}
      renderItem = {({item, index}) => {
        return (
          <ReposDataFlatList
          item={item} index={index} navigation={this.props.navigation}
          >
          </ReposDataFlatList>
        );
      }}
      keyExtractor={item => item._id}
      onEndReached={this.allAssetshandleLoadMore}
      onEndReachedThreshold={0}
      onEndThreshold={0}
      >
      </FlatList>
    )
  }else{
    return(
      <View style={{
        flex: 1,
        alignItems: "center",
      }}>
      <Text style={{
        color: '#AA00FF',
        fontSize: 20,
        fontWeight: '700',
        marginTop: 100
      }}>Please search any github repo</Text>
      </View>
    )
  }
}

allAssetshandleLoadMore = () => {
  this.setState({
    page: this.state.page + 1,
  })
  this.GetRepositories()
};

async GetRepositories(){
  this.setState({
    isLoaderEnabled: true
  })
  try {
    fetch(`${ApiService.SearchRepositories}?q=topic:${this.state.searchGitRep}&type=all&page=${this.state.page}&per_page=${this.state.count}`, {
      method: 'GET',
    }).then((response) => response.json())
    .then(async (responseJson) => {
      this.setState({
        isLoaderEnabled: false
      })
      if (responseJson.message == 'Not Found'){
        Alert.alert(
          `Not Found!!`,
          `Repo not found, please try for another.`,
          [
            {text: 'OK', onPress: () => console.log("not found")},
          ],
          { cancelable: false }
        )
      }else{
        this.setState({
          reposData: this.state.page === 1 ? responseJson.items : [...this.state.reposData, ...responseJson.items],
        })
      }
    })
    .catch((error) => {
      this.setState({
        isLoaderEnabled: false
      })
      console.log(error)
    });
  } catch (error) {

    this.setState({
      isLoaderEnabled: false
    })
    console.log(error)
  }
}

}

export default HomeScreen;
