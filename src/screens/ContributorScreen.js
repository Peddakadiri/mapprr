import React, { Component } from 'react';
import {
  View,
  FlatList,
  Text,
  ScrollView,
  Alert,
  Image,
} from 'react-native';
import ApiService from '../ApiService';
import { SearchBar } from 'react-native-elements';
import ContributorRepoTable from '../data/ContributorRepoTable';

class ContributorScreen extends React.PureComponent{

  //Default Navigation Of Screen
  static navigationOptions = {
    headerTitle:'GitRepo',
    headerTitleStyle: {
      color:'#22A7F0'
    },
    headerStyle: {
      backgroundColor:'white'
    },
    headerTintColor: 'black',
  }

  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      count: 10,
      reposData: [],
    }
  }

  componentDidMount() {
    this.GetContributorRepos()
  }

  render() {
    return(
      <ScrollView style={{
        backgroundColor: 'white'
      }}>
      <View style={{
        flex:1,
        backgroundColor: 'white'
      }}>
      <Text style={{
        fontSize: 30,
        margin: 10,
        fontWeight: 'bold'
      }}> {this.props.navigation.state.params.repoName.login} </Text>
      <Image
      style={{
        width: 400,
        height: 250,
        alignSelf: 'center',
        resizeMode:"cover",
        borderRadius: 20,
        borderWidth: 1,
        marginTop: 10
      }}
      source={ {uri: this.props.navigation.state.params.repoName.avatar_url}}
      />
      <Text style={{
        fontSize: 30,
        margin: 10,
        fontWeight: 'bold'
      }}> Repo List </Text>
      </View>
      {this._renderContributors()}
      </ScrollView>
    )
  }

  _renderContributors(){
    if ((this.state.reposData != null) && (this.state.reposData.length > 0)){
      return(
        <FlatList style={{backgroundColor: 'white'}}
        data={this.state.reposData}
        renderItem = {({item, index}) => {
          return (
            <ContributorRepoTable
            item={item} index={index} navigation={this.props.navigation}
            >
            </ContributorRepoTable>
          );
        }}
        keyExtractor={item => item._id}
        onEndReached={this.allAssetshandleLoadMore}
        onEndReachedThreshold={0}
        onEndThreshold={0}
        >
        </FlatList>
      )
    }else{
      return(
        <Text style={{
          color: '#95A5A6',
          justifyContent: 'center',
          fontSize: 20,
          fontWeight: '700',
          textAlign: 'center',
          marginTop: 50
        }}>Please wait...</Text>
      )
    }
  }

  _renderDealsList(){
    if ((this.state.reposData != null) && (this.state.reposData.length > 0)){
      return(
        <FlatList
        data={this.state.reposData}
        renderItem = {({item, index}) => {
          return (
            <ContributorRepoTable
            item={item} index={index} navigation={this.props.navigation}
            >
            </ContributorRepoTable>
          );
        }}
        keyExtractor={item => item._id}
        onEndReached={this.allAssetshandleLoadMore}
        onEndReachedThreshold={0}
        onEndThreshold={0}
        >
        </FlatList>
      )
    }else{
      return(
        <Text style={{
          color: '#95A5A6',
          justifyContent: 'center',
          fontSize: 20,
          fontWeight: '700',
          textAlign: 'center',
          marginTop: 50
        }}>Please wait...</Text>
      )
    }
  }

  allAssetshandleLoadMore = () => {
    this.setState({
      page: this.state.page + 1,
    })
    this.GetContributorRepos()
  };

  async GetContributorRepos(){
    try {
      fetch(`${ApiService.GetContributorRepos}${this.props.navigation.state.params.repoName.login}/repos?page=${this.state.page}&per_page=${this.state.count}`, {
        method: 'GET',
      }).then((response) => response.json())
      .then(async (responseJson) => {
        if (responseJson.message == 'Not Found'){
          Alert.alert(
            `Not Found!!`,
            `Repo not found, please try for another.`,
            [
              {text: 'OK', onPress: () => console.log("not found")},
            ],
            { cancelable: false }
          )
        }else{
          this.setState({
            reposData: this.state.page === 1 ? responseJson : [...this.state.reposData, ...responseJson],
          })
        }
      })
      .catch((error) => {
        console.log(error)
      });
    } catch (error) {
      console.log(error)
    }
  }
}

export default ContributorScreen;
