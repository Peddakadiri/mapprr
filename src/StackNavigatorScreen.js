import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import HomeScreen from './screens/HomeScreen'
import RepoDetailsScreen from './screens/RepoDetailsScreen'
import ContributorScreen from './screens/ContributorScreen'

const StackNavigatorScreen = StackNavigator({
    HomeScreen: { screen: HomeScreen},
    RepoDetailsScreen: {screen: RepoDetailsScreen},
    ContributorScreen: {screen: ContributorScreen},
})

export default StackNavigatorScreen
